### Shinobi MQTT module
Connecting [Shinobi CCTV](https://shinobi.video/) to your [MQTT](http://mqtt.org/) broker of choice!

The bridge works two ways:
- add triggers from MQTT topics to videos and 
- receive triggers from monitors in MQTT.

#### Installation

_To update your module version, execute exactly same steps._ 

Login to you system with a terminal and go to the Shinobi home directory. In most cases at /home/Shinobi:
```bash
cd /home/Shinobi
```

Modify conf.json to include a mqtt section, see example configuration under section configuration:
```bash
sudo nano conf.json
```

Download the MQTT module into the customAutoLoad directory:
```bash
sudo curl -o ./libs/customAutoLoad/mqtt.js https://gitlab.com/geerd/shinobi-mqtt/raw/master/mqtt.js
```

Install the mqtt dependencies:
```bash
sudo npm install mqtt
```

Restart Shinobi:
```bash
sudo HOME=/root pm2 restart camera
```

After these steps Shinobi should successfully log in to your MQTT server and when the triggerTopics are configured also subscribe to the configured triggerTopics. To troubleshoot set verbose to true in conf.json and view the log using:
```bash
sudo HOME=/root pm2 log camera --lines=100
```

#### Configuration

You can enable the MQTT integration by adding a "mqtt" section to the main `conf.json` file.
This can be done either via Superuser UI or by directly editing the file.
Below goes a sample section with default settings and all options explained.

```
{
 ...
 "mqtt": {
    "verbose": false,
    "url": "mqtt://localhost:1883",
    "mqtt_options": {
      "username": "xxxxx",
      "password": "xxxxx" 
    },
    "topic": "shinobi",
    "filterplugs": ["pir1", "pir2", ...],
    "toMqttMessage": {
      "key": "key",
      "name": "name",
      "details": "details",
      "currentTimestamp": "currentTimestamp",
      "plug": "plug",
      "name": "mon.name"
    },
    "triggerTopics": {
      "motionsensor/pir1/out": {
        "monitorid": "monitorid",
        "groupkey": "groupkey"
      }, 
      {...}
    }
 },
 ...
}
```

* _verbose_ - enables a lot more log output, used mainly for debugging
* _url_ - a [URL to your MQTT broker](https://github.com/mqtt/mqtt.github.io/wiki/URI-Scheme) which will receive Shinobi events 
* _topic_ - root MQTT topic where all events will go
* _username_ the username to authenticate with the mqtt server
* _password_ the password to authenticate with the mqtt server
* _filterplugs_ filter out events with plugs defined in this array, usefull to prevent triggers originating from mqtt itself
* _toMqttMessage_ object to define message structure
* _triggerTopics_ to define the topics to subscribe to and receive triggers from mqtt which are stored in Shinobi. This can be usefull to use external sensors (e.g. pir sensors).

You can also specify other mqtt_options used to connect to the mqtt server. For a complete list of options see: [URL to MQTT Connect options](https://github.com/mqttjs/MQTT.js#client)


#### Message details

Currently only _trigger_ forwarding is supported. Event messages are composed out of Shinobi internal events and gets 
posted to the the topic of the following format:
> <config_prefix>/<user_group_key>/<monitor_id>/<event_type>

Here is an example of message topic and body:
> shinobi/f1VavFNaGn/g0Z7dK6Mfi/trigger
```json
{
  "name": "livingroom",
  "details": {
    "plug": "Yolo",
    "name": "yolo",
    "reason": "object",
    "matrices": [
      {
        "x": 521.9678344726562,
        "y": 182.34503173828125,
        "width": 253.935302734375,
        "height": 777.6563720703125,
        "tag": "person",
        "confidence": 62.821685791015625
      }
    ],
    "imgHeight": 480,
    "imgWidth": 640
  },
  "currentTimestamp": "2019-11-23T22:16:02+01:00",
  "plug": "Yolo"
}
``` 
The message body can be configured changing the default config value for _toMqttMessage_ 

#### ToDo list
* Implement other handlers like "no motion", monitor start, monitor stop, monitor crash, camera ping failure
* Debouncing option
